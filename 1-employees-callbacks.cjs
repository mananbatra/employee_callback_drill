/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/

const fs=require('fs');
const path=require('path');

const data = {
    "employees": [
        {
            "id": 23,
            "name": "Daphny",
            "company": "Scooby Doo"
        },
        {
            "id": 73,
            "name": "Buttercup",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 93,
            "name": "Blossom",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 13,
            "name": "Fred",
            "company": "Scooby Doo"
        },
        {
            "id": 89,
            "name": "Welma",
            "company": "Scooby Doo"
        },
        {
            "id": 92,
            "name": "Charles Xavier",
            "company": "X-Men"
        },
        {
            "id": 94,
            "name": "Bubbles",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 2,
            "name": "Xyclops",
            "company": "X-Men"
        }
    ]
}

const dataFilePath=path.join(__dirname,'data.json');

function writeDataFile(writeFilePath,data){

    fs.writeFile(writeFilePath,JSON.stringify(data), (error) =>{

        if(error){

            console.log(error);
        }else{
            readDataFile(dataFilePath)
        }

    })
};

function readDataFile(path){

    fs.readFile(path,'utf8',(error,data) =>{

        if(error)
        {
            console.log(error)
        }else{
            retieveDataFromID(data);
        }
    })
}

function writeSolutionFile(writeFilePath,data,cb,originalData){

    fs.writeFile(writeFilePath,JSON.stringify(data,null,2), (error) =>{

        if(error){

            console.log(error);
        }else{
            if(cb===null)
            {
                return;
            }else
            {
                cb(originalData);
            }
           
        }

    })
};

function retieveDataFromID(data){

    const employeeData=JSON.parse(data);
    //console.log(typeof employeeData);
    const employeeWithID=employeeData.employees.filter((employee) =>
    {
        return employee.id===2 || employee.id===23 || employee.id===13
    })
    const solutionFile1=path.join(__dirname,'solution1.json')
    writeSolutionFile(solutionFile1,employeeWithID,groupData,data);
}


function groupData(data){

    let employeeData=JSON.parse(data);
    //console.log(employeeData);
    let groupData=employeeData.employees.reduce((group, element) =>
    {
        if(element.company in group){
            group[element.company].push({"id": element.id,"name": element.name,"company": element.company})
        }else{
            group[element.company]=[]
            group[element.company].push({"id": element.id,"name": element.name,"company": element.company})
        }
        return group
    },{});

    //console.log(groupData);

    const solutionFile2=path.join(__dirname,'solution2.json')
    writeSolutionFile(solutionFile2,groupData,employeeGroupPowerpuff,data);
}

function employeeGroupPowerpuff(data){

    const employeeData=JSON.parse(data);
    //console.log(typeof employeeData);
    const employeeWithGroup=employeeData.employees.filter((employee) =>
    {
        return employee.company=="Powerpuff Brigade";
    })
    const solutionFile3=path.join(__dirname,'solution3.json')
    writeSolutionFile(solutionFile3,employeeWithGroup,deleteID,data);
    
}

function deleteID(data) {
    const employeeData = JSON.parse(data);
    const employeeWithID = employeeData.employees.filter((employee) => {
        return employee.id !== 2;
    });
    const solutionFile4 = path.join(__dirname, 'solution4.json');
    writeSolutionFile(solutionFile4, employeeWithID, sortData, data);
}

function sortData(data) {
    const employeeData = JSON.parse(data);
    employeeData.employees.sort((a, b) => {
        if (a.company === b.company) {
            return a.id - b.id;
        }
        return a.company.localeCompare(b.company);
    });
    const solutionFile5 = path.join(__dirname, 'solution5.json');
    writeSolutionFile(solutionFile5, employeeData, swapPositions, data);
}

function swapPositions(data) {
    const employeeData = JSON.parse(data);
    const index92 = employeeData.employees.findIndex((employee) => employee.id === 92);
    const index93 = employeeData.employees.findIndex((employee) => employee.id === 93);

    if (index92 !== -1 && index93 !== -1) {
        [employeeData.employees[index92], employeeData.employees[index93]] = [
            employeeData.employees[index93],
            employeeData.employees[index92],
        ];
    }

    const solutionFile6 = path.join(__dirname, 'solution6.json');
    writeSolutionFile(solutionFile6, employeeData, addBirthday, data);
}

function addBirthday(data) {
    const employeeData = JSON.parse(data);
    const currentDate = new Date();

    employeeData.employees.map((employee) => {
        if (employee.id % 2 === 0) {
            employee.birthday = currentDate.toISOString().slice(0, 10);
        }
        return employee;
    });

    const solutionFile7 = path.join(__dirname, 'solution7.json');
    writeSolutionFile(solutionFile7, employeeData, null, data);
}

writeDataFile(dataFilePath, data);
